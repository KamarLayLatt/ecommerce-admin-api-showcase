<?php

namespace Database\Factories;

use App\Models\Order;
use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\OrderItem>
 */
class OrderItemFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            // 'order_id' => Order::factory()->create()->id,
            'product_id' => Product::factory()->create()->id,
            'quantity' => fake()->numberBetween(1, 20),
            'subtotal' => fake()->numberBetween(1000, 10000)
        ];
    }
}
