<?php

namespace App\Services;

use App\Models\OrderItem;
use App\Services\OrderService;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class OrderItemService
{
    protected $orderService, $productService;

    public function __construct(OrderService $orderService, ProductService $productService)
    {
        $this->orderService = $orderService;
        $this->productService = $productService;
    }

    public function getList($order_id)
    {
        return OrderItem::with('product')->where('order_id', $order_id)->get();
    }

    public function create($order_id, $fields): OrderItem
    {
        $orderItem = OrderItem::create([
            'product_id' => $fields['product_id'],
            'order_id' => $order_id,
            'quantity' => $fields['quantity'],
            'subtotal' => $this->productService->getDetailById($fields['product_id'])->price * $fields['quantity']
        ]);

        return $this->getDetailById($orderItem->id);
    }

    public function getDetailById(String $id): OrderItem
    {
        try {
            return OrderItem::with('order', 'product')->findOrFail($id);
        } catch (ModelNotFoundException $exception) {
            abort(404, 'Order item not found');
        }
    }

    public function update($orderItemId, $fields): OrderItem
    {
        $orderItem = $this->getDetailById($orderItemId);

        $orderItem->update([
            'quantity' => $fields['quantity'],
            'subtotal' => $orderItem->product->price * $fields['quantity']
        ]);

        return $this->getDetailById($orderItem->id);
    }

    public function cancel($orderItemId, $status)
    {
        $orderItem = $this->getDetailById($orderItemId);
        $orderItem->update([
            'is_removed' => $status
        ]);
    }
}
