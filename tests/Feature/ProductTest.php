<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Product;
use App\Models\Category;

class ProductTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    /**
     * @testdox POST - /api/products
     */
    public function test_product_create_api(): void
    {
        $request = [
            'name' => $this->faker->name,
            'price' => $this->faker->numberBetween(100, 10000),
            'stock_quantity' => $this->faker->numberBetween(0, 100),
            'description' => $this->faker->text(),
            'category_id' => Category::factory()->create()->id,
            'image_file_name' => 'image1.jpg',
        ];
        $response = $this->postJson('/api/products', $request);
        $response->assertStatus(201);
    }

    /**
     * @testdox GET - /api/products
     */
    public function test_product_list_api(): void
    {
        $response = $this->getJson('/api/products');
        $response->assertStatus(200);
    }

    /**
     * @testdox GET - /api/products/{slug}
     */
    public function test_product_show_api(): void
    {
        $product = Product::factory()->create();
        $response = $this->getJson('/api/products/' . $product->slug);
        $response->assertStatus(200);
    }

    /**
     * @testdox PUT - /api/products/{id}
     */
    public function test_product_update_api(): void
    {
        $product = Product::factory()->create();
        $request = [
            'name' => $this->faker->name,
            'price' => $this->faker->numberBetween(100, 10000),
            'stock_quantity' => $this->faker->numberBetween(0, 100),
            'description' => $this->faker->text(),
            'category_id' =>  Category::factory()->create()->id,
            'image_file_name' => 'image1.jpg',
        ];
        $response = $this->putJson('/api/products/' . $product->id, $request);
        $response->assertStatus(200);
    }

    /**
     * @testdox DELETE - /api/products/{id}
     */
    public function test_product_delete_api(): void
    {
        $product = Product::factory()->create();
        $response = $this->deleteJson('/api/products/' . $product->id);
        $response->assertStatus(200);
    }
}
