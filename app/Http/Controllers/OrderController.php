<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Order;
use App\Http\Resources\OrderResource;
use App\Http\Requests\OrderUpdateRequest;
use App\Services\OrderService;

class OrderController extends Controller
{
    protected $orderService;

    public function __construct(OrderService $orderService)
    {
        $this->orderService = $orderService;
    }
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        return response()->json([
            'message' => 'Order list retrieved successfully',
            'orders' => OrderResource::collection($this->orderService->getList($request->all()))->response()->getData()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     */
    public function show(string $order_no)
    {
        $order = $this->orderService->getDetailByOrderNo($order_no);
        return response()->json(
            [
                'message' => 'Order detail retrieved successfully',
                'order' => new OrderResource($order)
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(OrderUpdateRequest $request, string $id)
    {
        $order = Order::find($id) ?? abort(404, 'Order not found');
        $order->update($request->all());
        return response()->json([
            'message' => 'The order has been updated.'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
