<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;

class RolePermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        Permission::create(['name' => 'create_category']);
        Permission::create(['name' => 'update_category']);
        Permission::create(['name' => 'view_category']);
        Permission::create(['name' => 'delete_category']);

        $role = Role::create(['name' => 'editor']);

        $role->syncPermissions(['create_category', 'update_category', 'view_category', 'delete_category']);

        $user = User::findOrFail(1);
        $user->assignRole('editor');
    }
}
