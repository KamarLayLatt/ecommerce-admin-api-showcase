<?php

use Illuminate\Support\Str;

if (!function_exists('slugWithNumber')) {
    function slugWithNumber($data)
    {
        return Str::slug($data, "-") . '-' . (int)(microtime(true) * 1000);
    }
}
