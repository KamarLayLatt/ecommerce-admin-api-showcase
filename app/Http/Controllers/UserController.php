<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\UserService;
use App\Http\Resources\UserResource;
use App\Http\Requests\UserCreateRequest;
use App\Http\Requests\UserUpdateRequest;

class UserController extends Controller
{
    protected $userService;

    public function __construct(UserService $userService)
    {
        $this->userService = $userService;
    }

    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        return response()->json([
            'message' => 'User list retrieved successfully',
            'users' => UserResource::collection($this->userService->getList($request->all()))->response()->getData()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(UserCreateRequest $request)
    {
        $user = $this->userService->create($request->all());
        return response()->json([
            'message' => 'The user has been successfully added.',
            'user' => new UserResource($user)
        ]);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $slug)
    {
        $user = $this->userService->getDetailBySlug($slug);
        return response()->json(
            [
                'message' => 'User detail retrieved successfully',
                'user' => new UserResource($user)
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UserUpdateRequest $request, string $id)
    {
        $user = $this->userService->updateById($id, $request->all());
        return response()->json([
            'message' => 'The user has been successfully updated.',
            'user' => new UserResource($user)
        ]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $this->userService->deleteById($id);
        return response()->json([
            'message' => 'The user has been successfully deleted.',
        ]);
    }
}
