<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;
use App\Http\Resources\OrderItemResource;

class OrderResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @return array<string, mixed>
     */
    public function toArray(Request $request): array
    {
        return [
            'id' => $this->id,
            'order_no' => $this->order_no,
            'order_date' => $this->order_date,
            'user' =>  new UserResource($this->whenLoaded('user')),
            'total_amount' => $this->total_amount,
            'shipping_address' => $this->shipping_address,
            'order_status' => $this->order_status,
            'payment_status' => $this->payment_status,
            'order_items' => OrderItemResource::collection($this->whenLoaded('orderItems')),
        ];
    }
}
