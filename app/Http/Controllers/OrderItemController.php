<?php

namespace App\Http\Controllers;

use App\Http\Requests\OrderItemCreateRequest;
use App\Http\Requests\OrderItemUpdateRequest;
use App\Http\Resources\OrderItemResource;
use Illuminate\Http\Request;
use App\Services\OrderItemService;

class OrderItemController extends Controller
{
    protected $orderItemService;

    public function __construct(OrderItemService $orderItemService)
    {
        $this->orderItemService = $orderItemService;
    }

    /**
     * Display a listing of the resource.
     */
    public function index($id)
    {
        $orderItems = $this->orderItemService->getList($id);
        return response()->json([
            'message' => 'success',
            'order_items' => OrderItemResource::collection($orderItems)
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(OrderItemCreateRequest $request, $id)
    {
        $orderItem = $this->orderItemService->create($id, $request->all());
        return response()->json([
            'message' => 'success',
            'order_item' => new OrderItemResource($orderItem)
        ]);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(OrderItemUpdateRequest $request, string $orderId, string $orderItemId)
    {
        $orderItem = $this->orderItemService->update($orderItemId, $request->all());
        return response()->json([
            'message' => 'success',
            'order_item' => new OrderItemResource($orderItem)
        ]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Request $request, string $orderId, string $orderItemId)
    {
        $this->orderItemService->cancel($orderItemId, $request->is_removed);
        return response()->json([
            'message' => 'success'
        ]);
    }
}
