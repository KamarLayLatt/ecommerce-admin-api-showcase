<?php

namespace App\Http\Controllers;

use App\Services\AuthService;
use App\Http\Requests\LoginRequest;
use App\Http\Controllers\Controller;
use App\Http\Requests\ForgotPasswordRequest;
use App\Http\Requests\ResetPasswordRequest;
use App\Http\Resources\UserResource;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    protected $authService;

    public function __construct(AuthService $authService)
    {
        $this->authService = $authService;
    }

    public function login(LoginRequest $request)
    {
        $loginResult = $this->authService->login($request->only('email', 'password'));

        if ($loginResult) {
            return response()->json([
                'token' => $loginResult['token'],
                'user' => new UserResource($loginResult['user'])
            ]);
        } else {
            abort(401, 'The email address or password that you\'ve entered doesn\'t match any account.');
        }
    }

    public function logout()
    {
        $this->authService->logout();
        return response()->json(['message' => 'Logout successful'], 200);
    }

    public function getUser()
    {
        return response()->json(new UserResource(Auth::user()));
    }

    // public function forgotPassword(ForgotPasswordRequest $request)
    // {
    //     $forgot = $this->authService->forgotPassword($request->email);

    //     if (config('app.env') === 'production') {
    //         return response()->json([
    //             'message' => "Password reset instructions have been sent to your email address.",
    //         ]);
    //     } else {
    //         return response()->json([
    //             'message' => "Password reset instructions have been sent to your email address.",
    //             'forgot_link' => $forgot['forgot_link'],
    //             'token' => $forgot['token']
    //         ]);
    //     }
    // }

    // public function resetPassword(ResetPasswordRequest $request)
    // {
    //     $this->authService->resetPassword($request->all());
    //     return response()->json([
    //         'message' => "Your password has been successfully reset.",
    //     ]);
    // }
}
