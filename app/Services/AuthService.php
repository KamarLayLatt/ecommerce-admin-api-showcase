<?php

namespace App\Services;

use App\Models\User;
use Illuminate\Support\Str;
use App\Mail\ForgotPassword;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;

class AuthService
{
    public function login($credentials)
    {
        // Attempt to authenticate the user
        if (Auth::attempt($credentials)) {
            // Authentication successful
            $user = Auth::user();
            $token = $user->createToken('auth_token')->plainTextToken;
            return ['token' => $token, 'user' => $user];
        }

        // Authentication failed
        return null;
    }

    public function logout()
    {
        // Revoke the request user's token
        Auth::user()->currentAccessToken()->delete();

        // Revoke for all user's token
        // Auth::user()->tokens()->delete();
    }

    // public function forgotPassword(String $email)
    // {
    //     $token = Str::random(64);
    //     try {
    //         User::where('email', $email)->sole();
    //     } catch (\Throwable $th) {
    //         abort(404, 'Invalid Mail.');
    //     }

    //     $reset_password = DB::table('password_reset_tokens')->where('email', $email);
    //     if ($reset_password->first()) {
    //         $reset_password->delete();
    //     }
    //     DB::table('password_reset_tokens')->insert([
    //         'email' => $email,
    //         'token' => $token,
    //         'created_at' => Carbon::now()
    //     ]);
    //     $front_app_url = config('app.frontend_url');
    //     $forgot_link = "{$front_app_url}/forgot-password/{$token}";

    //     Mail::to($email)->send(new ForgotPassword($forgot_link));

    //     return [
    //         'forgot_link' => $forgot_link,
    //         'token' => $token
    //     ];
    // }

    // public function resetPassword($field): void
    // {
    //     $reset_password = DB::table('password_reset_tokens')->where('token', $field['token']);

    //     $reset_password_data = $reset_password->first();


    //     if (!$reset_password_data) {
    //         abort(404, 'Invalid Token');
    //     }

    //     $expirationTime = Carbon::parse($reset_password_data->created_at)->addMinutes(15);

    //     if (Carbon::now()->gt($expirationTime)) {
    //         // Token has expired
    //         abort(401, 'Token has expired');
    //     }

    //     $user = User::where('email', $reset_password_data->email);
    //     if (!$user->first()) {
    //         abort(404, 'User not found');
    //     }
    //     $user->update([
    //         'password' => Hash::make($field['password'])
    //     ]);
    //     $reset_password->delete();
    // }
}
