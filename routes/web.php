<?php

use Illuminate\Support\Facades\Route;
use App\Services\OpenAI;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

// Route::get('test', function () {
//     $openai = new OpenAI();
//     return $openai->generateText("List Popular Actor in 2023.");
// });
