<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $name = 'Super Admin';
        \App\Models\User::factory()->create([
            'name' => $name,
            'email' => 'superadmin@gmail.com',
            'slug' => slugWithNumber($name)
        ]);
    }
}
