<?php

namespace App\Services;

use App\Models\Category;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class CategoryService
{
    public function getList(array $args): LengthAwarePaginator | Collection
    {
        $model = Category::query();
        if (isset($args['search'])) {
            $this->filter($model, $args['search']);
        }
        if (isset($args['page'])) {
            // If pagination is requested, return a LengthAwarePaginator instance
            return $model->paginate($args['limit'] ?? 10);
        } else {
            // If no pagination is requested, return a Collection of results
            return isset($args['limit']) ? $model->take($args['limit'])->get() : $model->get();
        }
    }

    public function filter(Builder $model, string $search): void
    {
        $model->orWhere('name', 'like', '%' . $search . '%')->orWhere('description', 'like', '%' . $search . '%');
    }

    public function create($fields)
    {
        $category = Category::create($fields + ['slug' =>  slugWithNumber($fields['name'])]);
        return $this->getDetailById($category->id);
    }

    public function getDetailBySlug(String $slug): Category
    {
        try {
            return Category::where('slug', $slug)->sole();
        } catch (ModelNotFoundException $exception) {
            abort(404, 'Category not found');
        }
    }

    public function getDetailById(String $id): Category
    {
        try {
            return Category::findOrFail($id);
        } catch (ModelNotFoundException $exception) {
            abort(404, 'Category not found');
        }
    }

    public function updateById(int $id, array $fields): Category
    {
        $category = $this->getDetailById($id);
        $category->update($fields + ['slug' => slugWithNumber($fields['name'])]);
        return $this->getDetailById($category->id);
    }

    public function deleteById(int $id): Void
    {
        $category = Category::with('products')->findOrFail($id);
        if (!count($category->products)) {
            $this->getDetailById($id)->delete();
        } else {
            abort(400, 'Cannot delete category with associated products.');
        }
    }
}
