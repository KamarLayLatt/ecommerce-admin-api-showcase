<?php

namespace App\Services;

use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class UserService
{
    /**
     * Get a list of users based on the provided arguments.
     *
     * @param array $args The arguments for filtering and pagination.
     *   - 'search' (string|null): Search relationships id, name and email.
     *   - 'page' (int): Page number of users for pagination.
     *   - 'limit' (int|null): The number of users to retrieve per page (optional, default is 10).
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Database\Eloquent\Collection
     */
    public function getList(array $args): LengthAwarePaginator | Collection
    {
        $model = User::query();
        if (isset($args['search'])) {
            $this->filter($model, $args['search']);
        }
        if (isset($args['page'])) {
            // If pagination is requested, return a LengthAwarePaginator instance
            return $model->paginate($args['limit'] ?? 10);
        } else {
            // If no pagination is requested, return a Collection of results
            return isset($args['limit']) ? $model->take($args['limit'])->get() : $model->get();
        }
    }

    /**
     * Apply filters to the given query builder for User models.
     *
     * @param \Illuminate\Database\Eloquent\Builder $model
     *   The query builder instance for the User model.
     * @param string $search
     *   Filter by email, name and id.
     *
     * @return void
     */
    public function filter(Builder $model, string $search): void
    {
        $model->where('id', $search)->orWhere('name', 'like', '%' . $search . '%')->orWhere('email', 'like', '%' . $search . '%');
    }

    /**
     * Get the details of a user by its unique slug.
     *
     * @param string $slug
     *   The unique slug identifying the user.
     *
     * @return \App\Models\User
     *   The user model representing the details.
     *
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     *   Thrown if the user with the specified slug is not found.
     */
    public function getDetailBySlug(String $slug): User
    {
        try {
            return User::where('slug', $slug)->sole();
        } catch (ModelNotFoundException $exception) {
            abort(404, 'User not found');
        }
    }

    /**
     * Get the details of a user by its unique id.
     *
     * @param string $id
     *   The unique id identifying the user.
     *
     * @return \App\Models\User
     *   The user model representing the details.
     *
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     *   Thrown if the user with the specified id is not found.
     */
    public function getDetailById(String $id): User
    {
        try {
            return User::findOrFail($id);
        } catch (ModelNotFoundException $exception) {
            abort(404, 'User not found');
        }
    }

    /**
     * Create a new user with the provided fields.
     *
     * @param array $fields The fields for creating the user.
     *                      Expected keys: 'name', 'email.
     *
     * @return User The created user instance.
     */
    public function create(array $fields): User
    {
        $user =  User::create([
            'name' => $fields['name'],
            'slug' => slugWithNumber($fields['name']),
            'email' => $fields['email'],
            'password' => Hash::make($fields['password']),
        ]);

        return $this->getDetailById($user->id);
    }

    /**
     * Update an existing user in the database.
     *
     * @param int   $id     The identifier of the user to be updated.
     * @param array $fields The fields to be updated.
     *                      Expected keys: 'name', 'description
     *
     * @return User The updated user instance.
     *
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException If the user with the given ID is not found.
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException If the user is not found, a 404 response is thrown.
     */
    public function updateById(int $id, array $fields): User
    {
        $user = $this->getDetailById($id);
        $user->update([
            'name' => $fields['name'],
            'slug' => slugWithNumber($fields['name']),
            'email' => $fields['email']
        ]);

        if ($fields['password']) {
            $user->password = Hash::make($fields['password']);
            $user->save();
        }

        return $this->getDetailById($user->id);
    }

    /**
     * Delete a user by its identifier.
     *
     * @param int $id The identifier of the user to be deleted.
     *
     * @return void
     *
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException If the user with the given ID is not found.
     */
    public function deleteById(int $id): Void
    {
        $this->getDetailById($id)->delete();
    }

    /**
     * Activate or deactivate a user based on the provided status.
     *
     * @param int  $user_id The ID of the user to activate or deactivate.
     * @param bool $status  The activation status (true for activation, false for deactivation).
     *
     * @return void
     */
    public function activate(int $user_id, bool $status): Void
    {
        $user = $this->getDetailById($user_id);
        $user->is_active = $status;
        $user->save();
    }
}
