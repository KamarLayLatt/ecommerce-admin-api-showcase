<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;

class RoleController extends Controller
{
    public function index()
    {
        $roles = Role::all();
        return response()->json(['roles' => $roles], 200);
    }

    public function store(Request $request)
    {
        $role = Role::create(['name' => $request->name]);
        return response()->json([
            'message' => "Successfully added role",
            'role' => $role
        ]);
    }

    public function show($id)
    {
        $role = Role::findById($id);
        return response()->json([
            'message' => "Successfully retrived role",
            'role' => $role->toArray() + ['permissions' => $role->permissions()->orderBy('name')->get()]
        ]);
    }

    public function update(Request $request, $id)
    {
        $role = Role::findById($id);
        $role->update([
            'name' => $request->name
        ]);
        return response()->json([
            'message' => "Successfully update role",
            'role' => $role
        ]);
    }

    public function destroy($id)
    {
        DB::table('roles')->where('id', $id)->delete();
        return response()->json([
            'message' => "Successfully deleted role"
        ]);
    }

    public function syncPermissions(Request $request, $id)
    {
        $role = Role::findById($id);
        $role->syncPermissions($request->permissions);
        return response()->json([
            'message' => "Successfully update role",
            'role' => $role->toArray() + ['permissions' => $role->permissions()->orderBy('name')->get()]
        ]);
    }

    public function removePermissions(Request $request, $id)
    {
        $role = Role::findById($id);
        $role->revokePermissionTo($request->permissions);
        return response()->json([
            'message' => "Successfully update role",
            'role' => $role->toArray() + ['permissions' => $role->permissions()->orderBy('name')->get()]
        ]);
    }
}
