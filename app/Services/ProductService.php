<?php

namespace App\Services;

use App\Models\Product;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class ProductService
{
    /**
     * Get a list of products based on the provided arguments.
     *
     * @param array $args The arguments for filtering and pagination.
     *   - 'search' (string|null): Search products.
     *   - 'page' (int): Page number of products for pagination.
     *   - 'limit' (int|null): The number of products to retrieve per page (optional, default is 10).
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Database\Eloquent\Collection
     */
    public function getList(array $args): LengthAwarePaginator | Collection
    {
        $model = Product::query();
        if (isset($args['search'])) {
            $this->filter($model, $args['search']);
        }
        if (isset($args['page'])) {
            // If pagination is requested, return a LengthAwarePaginator instance
            return $model->paginate($args['limit'] ?? 10);
        } else {
            // If no pagination is requested, return a Collection of results
            return isset($args['limit']) ? $model->take($args['limit'])->get() : $model->get();
        }
    }

    /**
     * Apply filters to the given query builder for Product models.
     *
     * @param \Illuminate\Database\Eloquent\Builder $model
     *   The query builder instance for the Product model.
     * @param string $search
     *   Filter by email, name and id.
     *
     * @return void
     */
    public function filter(Builder $model, string $search): void
    {
        $model->where('id', $search)->orWhere('name', 'like', '%' . $search . '%')->orWhere('email', 'like', '%' . $search . '%');
    }

    /**
     * Get the details of a product by its unique slug.
     *
     * @param string $slug
     *   The unique slug identifying the product.
     *
     * @return \App\Models\Product
     *   The product model representing the details.
     *
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     *   Thrown if the product with the specified slug is not found.
     */
    public function getDetailBySlug(String $slug): Product
    {
        try {
            return Product::where('slug', $slug)->sole();
        } catch (ModelNotFoundException $exception) {
            abort(404, 'Product not found');
        }
    }

    /**
     * Get the details of a product by its unique id.
     *
     * @param string $id
     *   The unique id identifying the product.
     *
     * @return \App\Models\Product
     *   The product model representing the details.
     *
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     *   Thrown if the product with the specified id is not found.
     */
    public function getDetailById(String $id): Product
    {
        try {
            return Product::findOrFail($id);
        } catch (ModelNotFoundException $exception) {
            abort(404, 'Product not found');
        }
    }

    /**
     * Create a new product with the provided fields.
     *
     * @param array $fields The fields for creating the product.
     *                      Expected keys: 'name', 'email.
     *
     * @return Product The created product instance.
     */
    public function create(array $fields): Product
    {
        $product =  Product::create([
            'name' => $fields['name'],
            'slug' => slugWithNumber($fields['name']),
            'description' => $fields['description'],
            'price' => $fields['price'],
            'stock_quantity' => $fields['stock_quantity'],
            'category_id' => $fields['category_id']
        ]);

        return $this->getDetailById($product->id);
    }

    /**
     * Update an existing product in the database.
     *
     * @param int   $id     The identifier of the product to be updated.
     * @param array $fields The fields to be updated.
     *                      Expected keys: 'name', 'description
     *
     * @return Product The updated product instance.
     *
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException If the product with the given ID is not found.
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException If the product is not found, a 404 response is thrown.
     */
    public function updateById(int $id, array $fields): Product
    {
        $product = $this->getDetailById($id);
        $product->update([
            'name' => $fields['name'],
            'slug' => slugWithNumber($fields['name']),
            'description' => $fields['description'],
            'price' => $fields['price'],
            'stock_quantity' => $fields['stock_quantity'],
            'category_id' => $fields['category_id']
        ]);
        return $this->getDetailById($product->id);
    }

    /**
     * Delete a product by its identifier.
     *
     * @param int $id The identifier of the product to be deleted.
     *
     * @return void
     *
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException If the product with the given ID is not found.
     */
    public function deleteById(int $id): Void
    {
        $product = Product::with('orderItems')->findOrFail($id);
        if (!count($product->orderItems)) {
            $this->getDetailById($id)->delete();
        } else {
            abort(400, 'Cannot delete product with associated order items.');
        }
    }
}
