<?php

namespace App\Services;

use App\Models\Order;
use Illuminate\Support\Facades\Hash;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class OrderService
{
    /**
     * Get a list of orders based on the provided arguments.
     *
     * @param array $args The arguments for filtering and pagination.
     *   - 'search' (string|null): Search orders.
     *   - 'page' (int): Page number of orders for pagination.
     *   - 'limit' (int|null): The number of orders to retrieve per page (optional, default is 10).
     *
     * @return \Illuminate\Contracts\Pagination\LengthAwarePaginator|\Illuminate\Database\Eloquent\Collection
     */
    public function getList(array $args): LengthAwarePaginator | Collection
    {
        $model = Order::query();
        if (isset($args['search'])) {
            $this->filter($model, $args['search']);
        }
        if (isset($args['page'])) {
            // If pagination is requested, return a LengthAwarePaginator instance
            return $model->paginate($args['limit'] ?? 10);
        } else {
            // If no pagination is requested, return a Collection of results
            return isset($args['limit']) ? $model->take($args['limit'])->get() : $model->get();
        }
    }

    /**
     * Apply filters to the given query builder for Order models.
     *
     * @param \Illuminate\Database\Eloquent\Builder $model
     *   The query builder instance for the Order model.
     * @param string $search
     *   Filter by email, name and id.
     *
     * @return void
     */
    public function filter(Builder $model, string $search): void
    {
        $model->where('id', $search)->orWhere('name', 'like', '%' . $search . '%')->orWhere('email', 'like', '%' . $search . '%');
    }

    /**
     * Get the details of a order by its unique order no.
     *
     * @param string $order_no
     *   The unique order_no identifying the order.
     *
     * @return \App\Models\Order
     *   The order model representing the details.
     *
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     *   Thrown if the order with the specified order_no is not found.
     */
    public function getDetailByOrderNo(String $order_no): Order
    {
        try {
            return Order::with('orderItems', 'user')->where('order_no', $order_no)->sole();
        } catch (ModelNotFoundException $exception) {
            abort(404, 'Order not found');
        }
    }

    /**
     * Get the details of a order by its unique id.
     *
     * @param string $id
     *   The unique id identifying the order.
     *
     * @return \App\Models\Order
     *   The order model representing the details.
     *
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException
     *   Thrown if the order with the specified id is not found.
     */
    public function getDetailById(String $id): Order
    {
        try {
            return Order::with('orderItems')->findOrFail($id);
        } catch (ModelNotFoundException $exception) {
            abort(404, 'Order not found');
        }
    }

    /**
     * Update an existing order in the database.
     *
     * @param int   $id     The identifier of the order to be updated.
     * @param array $fields The fields to be updated.
     *                      Expected keys: 'name', 'description
     *
     * @return Order The updated order instance.
     *
     * @throws \Illuminate\Database\Eloquent\ModelNotFoundException If the order with the given ID is not found.
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException If the order is not found, a 404 response is thrown.
     */
    public function updateById(int $id, array $fields): Order
    {
        $order = $this->getDetailById($id);
        $order->update([
            'order_date' => $fields['order_date'],
            'total_amount' => $fields['total_amount'],
            'shipping_address' => $fields['shipping_address'],
            'order_status' => $fields['order_status'],
            'payment_status' => $fields['payment_status'],
        ]);
        return $this->getDetailById($order->id);
    }
}
