<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('order_no')->unique();
            $table->foreignId('user_id')->constrained();
            $table->timestamp('order_date');
            $table->decimal('total_amount', $precision = 8, $scale = 2)->default(0);
            $table->text('shipping_address')->nullable();
            $table->enum('order_status', ['pending', 'shipped', 'delivered', 'canceled']);
            $table->enum('payment_status', ['paid', 'pending', 'refunded']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('orders');
    }
};
