<?php

namespace App\Services;

use GuzzleHttp\Client;

class OpenAI
{
    protected $client;

    public function __construct()
    {
        $this->client = new Client([
            'base_uri' => 'https://api.openai.com/v1/',
            'headers' => [
                'Authorization' => 'Bearer ' . config('services.openai.api_key'),
                'x-ratelimit-limit-requests' => 60
            ],
        ]);
    }

    public function generateText($prompt)
    {
        $response = $this->client->post('chat/completions', [
            'json' => [
                "model" => "gpt-3.5-turbo",
                "messages" => [
                    [
                        "role" => "user",
                        "content" => "today date"
                    ]
                ],
            ],
        ]);

        return json_decode($response->getBody());
    }
}
