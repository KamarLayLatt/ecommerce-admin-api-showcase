<?php

namespace Tests\Feature;

use Tests\TestCase;
use App\Models\User;
use App\Models\Order;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class OrderTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    /**
     * @testdox GET - /api/orders
     */
    public function test_order_list_api(): void
    {
        $response = $this->getJson('/api/orders');
        $response->assertStatus(200);
    }

    /**
     * @testdox GET - /api/orders/{order_no}
     */
    public function test_order_show_api(): void
    {
        $order = Order::factory()->create();
        $response = $this->getJson('/api/orders/' . $order->order_no);
        $response->assertStatus(200);
    }

    /**
     * @testdox PUT - /api/orders/{id}
     */
    public function test_order_update_api(): void
    {
        $order = Order::factory()->create();
        $request = [
            "order_date" => "2023-11-10 15:03:56",
            "total_amount" => 5000,
            "shipping_address" => "425, Lathar street, Yangon",
            "order_status" => "canceled",
            "payment_status" => "paid"
        ];
        $response = $this->putJson('/api/orders/' . $order->id, $request);
        $response->assertStatus(200);
    }
}
