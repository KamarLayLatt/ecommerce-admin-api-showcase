<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\Category;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Product>
 */
class ProductFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        $name = fake()->name();
        return [
            'name' => $name,
            'slug' => slugWithNumber($name),
            'stock_quantity' => fake()->numberBetween(0, 100),
            'description' => fake()->text(),
            'category_id' => Category::factory()->create()->id,
            'image_file_name' => 'image1.jpg',
        ];
    }
}
