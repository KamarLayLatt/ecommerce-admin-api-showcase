<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;
use App\Models\Category;

class CategoryTest extends TestCase
{
    use RefreshDatabase, WithFaker;

    /**
     * @testdox POST - /api/categories
     */
    public function test_category_create_api(): void
    {
        $request = [
            'name' => $this->faker->name,
            'description' => $this->faker->text()
        ];
        $response = $this->postJson('/api/categories', $request);
        $response->assertStatus(201);
    }

    /**
     * @testdox GET - /api/categories
     */
    public function test_category_list_api(): void
    {
        $response = $this->getJson('/api/categories');
        $response->assertStatus(200);
    }

    /**
     * @testdox GET - /api/categories/{slug}
     */
    public function test_category_show_api(): void
    {
        $category = Category::factory()->create();
        $response = $this->getJson('/api/categories/' . $category->slug);
        $response->assertStatus(200);
    }

    /**
     * @testdox PUT - /api/categories/{id}
     */
    public function test_category_update_api(): void
    {
        $category = Category::factory()->create();
        $request = [
            'name' => $this->faker->name,
            'description' => $this->faker->text()
        ];
        $response = $this->putJson('/api/categories/' . $category->id, $request);
        $response->assertStatus(200);
    }

    /**
     * @testdox DELETE - /api/categories/{id}
     */
    public function test_category_delete_api(): void
    {
        $category = Category::factory()->create();
        $response = $this->deleteJson('/api/categories/' . $category->id);
        $response->assertStatus(200);
    }
}
