<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductCreateRequest;
use App\Http\Requests\ProductUpdateRequest;
use Illuminate\Http\Request;
use App\Models\Product;
use App\Http\Resources\ProductResource;
use App\Services\ProductService;

class ProductController extends Controller
{
    protected $productService;

    public function __construct(ProductService $productService)
    {
        $this->productService = $productService;
    }
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        return response()->json([
            'message' => 'Product list retrieved successfully',
            'products' => ProductResource::collection($this->productService->getList($request->all()))->response()->getData()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(ProductCreateRequest $request)
    {
        $product = $this->productService->create($request->all());
        return response()->json([
            'message' => 'The product has been successfully added.',
            'product' => new ProductResource($product)
        ]);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $slug)
    {
        $product = $this->productService->getDetailBySlug($slug);
        return response()->json(
            [
                'message' => 'Product detail retrieved successfully',
                'product' => new ProductResource($product)
            ]
        );
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(ProductUpdateRequest $request, string $id)
    {
        $product = $this->productService->updateById($id, $request->all());
        return response()->json([
            'message' => 'The product has been successfully updated.',
            'product' => new ProductResource($product)
        ]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $this->productService->deleteById($id);
        return response()->json([
            'message' => 'The product has been successfully deleted.',
        ]);
    }
}
