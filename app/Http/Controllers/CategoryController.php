<?php

namespace App\Http\Controllers;

use App\Models\Category;
use Illuminate\Http\Request;
use App\Services\CategoryService;
use App\Http\Resources\CategoryResource;
use App\Http\Requests\CategoryCreateRequest;
use App\Http\Requests\CategoryUpdateRequest;

class CategoryController extends Controller
{
    protected $categoryService;

    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    }
    /**
     * Display a listing of the resource.
     */
    public function index(Request $request)
    {
        $categories = $this->categoryService->getList($request->all());
        return response()->json([
            'message' => 'Category list retrieved successfully.',
            'categories' => CategoryResource::collection($categories)->response()->getData()
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(CategoryCreateRequest $request)
    {
        $category = $this->categoryService->create($request->all());
        return response()->json([
            'message' => 'The category has been successfully added.',
            'category' => new CategoryResource($category)
        ], 201);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $slug)
    {
        $category = $this->categoryService->getDetailBySlug($slug);
        return response()->json([
            'message' => 'The category detail has been retrived successfully.',
            'category' => new CategoryResource($category)
        ], 201);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(CategoryUpdateRequest $request, string $id)
    {
        $category = $this->categoryService->updateById($id, $request->all());
        return response()->json([
            'message' => 'The category has been updated.',
            'category' => new CategoryResource($category)
        ]);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        $this->categoryService->deleteById($id);
        return response()->json(["message" => "Category has been deleted successfully."]);
    }
}
